# include <queue>
# include <vector>
# include <string>
# include <algorithm>
# include <iostream>
# include <fstream>
# include <sstream>
# include <cmath>
# include <cstdlib>
# include <ctime>
# include <Rcpp.h>

# define DD_STEPSIZE 1
# define DD_ITER 30
# define DD_EPS 0.5e-2
# define DD_CHANGE 1
# define PMAX 10000
# define NUM_OF_BIN 50
# define ADJUST_OPTIMIZATION 1.15

using namespace Rcpp;
using namespace std;

struct RANK_SCORE
{
  double value;
  int index;
};

struct rr
{ 
  int a;
  int b;
  double c;
};

bool mycmp_double(double a, double b)
{
  return a>b;
}

bool mycmp_rr(const rr &x, const rr &y)
{ 
  return fabs(x.c) > fabs(y.c);
}

bool cmp_score(const RANK_SCORE &a, const RANK_SCORE &b)
{
  return (a.value > b.value);
}

bool cmp_absolute_score(const RANK_SCORE &a, const RANK_SCORE &b)
{
  return (fabs(a.value) > fabs(b.value));
}

struct node
{
  int i;
  int j;
  double val;
  node(int a, int b, double c):i(a), j(b), val(c){}
  node(){i=0; j=0; val=0;}
};

class compare
{
public:
  bool operator() (const node &n1, const node &n2) const
  {return n1.val > n2.val;}
};

void GENP(int n, double gamma, int *g, int recall, double fuzzy_level)
{
  /* Calculate the expected degree of each node according to power law distribution */

  int *bin;
  double *STORE;
  double sum = 0;
  double nodesum = 0;
  int index = 1;
  int mysize;
  int bin_number = NUM_OF_BIN;
  STORE = new double [PMAX+1];
  bin = new int[bin_number+1];

  if (gamma > 2.1)
    {
      mysize = PMAX;
    }
  else
    {
      mysize = bin_number;
    }

  for (int i=1; i<=mysize; ++i)
    {
      STORE[i] = pow(1.0/i,gamma);
    }
  for (int i=mysize; i>0; --i)
    {
      sum += STORE[i];
    }

  for (int i=mysize; i>0; --i)
    {

      nodesum += STORE[i];

      while (nodesum >= sum/bin_number)
        {
          bin[index] = i;
          ++index;
          nodesum -= sum/bin_number;
        }

      if (index > bin_number)
        {
          break;
        }
    }

  if (nodesum > 0)
    {
      bin[bin_number] = 1;
    }

  /* Calculate the expected degree of each node according to power law distribution */

  /* Rescale according to required recall */

  sum = 0;
  for (int i=1; i<bin_number+1; ++i)
    {
      sum += bin[i];
    }

  for (int i=1; i<n+1; ++i)
    {
      int slot;
      slot = 1.0*i/n*bin_number+1;

      if (slot > bin_number)
	{
	  slot = bin_number;
	}

      if (i < n/2)
	{
	  g[i] = 0.5 + fuzzy_level*bin[slot]/sum*recall/bin_number;
	}
      else
	{
	  g[i] = 0.5 + fuzzy_level/20.0*bin[slot]/sum*recall/bin_number;
	}

      if (g[i] > n-1)
	{
	  g[i] = n-1;
	}
    }

  delete []STORE;
  delete []bin;

  /* Rescale according to required recall */
}

void GENH(int n, double *h)
{
  for (int i=1; i<n; ++i)
    {
      h[i] = log(i+1)-log(i);
    }
}

void TH(int n, double *th, int recall, double tune_smooth)
{
  // Generate the H function

  double ratio1;
  double ratio2;
  double smooth;

  ratio1 = 2.0*recall/n/(n-1);
  ratio2 = (0.01-ratio1)/0.01;
  smooth = ratio2*(log10(n)+tune_smooth*ratio2)/2.0;

  if (smooth < 0.15)
    {
      smooth = 0.15;
    }

  if (smooth > 1.0)
    {
      smooth = 1.0;
    }

  for (int i=1; i<n+1; ++i)
    {
      th[i] = pow(log(i+1),smooth);
    }
}

void TG(int n, int *g, double *th, double *tg)
{
  for (int i=1; i<=n; ++i)
    {
      if (g[i] != 0)
	{
	  tg[n+1-i] = 1.0/th[g[i]];
	}
      else
	{
	  tg[n+1-i] = 1.1/th[1];
	}
    }
}

void CAL_SCORE(int n, double **M, double *h, struct RANK_SCORE *sort_score)
{
  priority_queue <double> pq;

  for (int i=1; i<n+1; ++i)
    {
      for (int j=1; j<n+1; ++j)
        {
          if (i != j)
            {
              pq.push(fabs(M[i][j]));
            }
        }

      sort_score[i].value = 0;
      sort_score[i].index = i;

      for (int j=1; j<n; ++j)
        {
          sort_score[i].value += pq.top()*h[j];
          pq.pop();
        }
    }
}

void ROW_SCORE(int n, int id, double *myans, double *M, double *h, double rho, double alpha, struct RANK_SCORE *myscore, double *old_h)
{
  alpha /= rho;

  int len;
  int *sumlen;
  double *mysum;
  double *ans;
  struct RANK_SCORE *myM;

  sumlen = new int[n+2];
  mysum = new double[n+2];
  ans = new double[n+2];
  myM = new RANK_SCORE[n+2];

  sumlen[0] = 0;
  mysum[0] = 0;
  len = 0;
  for (int i=1; i<id; ++i)
    { 
      myM[i].value = M[i];
      myM[i].index = i;
    }

  for (int i=id+1; i<n+1; ++i)
    { 
      myM[i-1].value = M[i];
      myM[i-1].index = i;
    }

  sort(myM+1,myM+n,cmp_absolute_score);

  for (int i=1; i<n; ++i)
    {
      mysum[i] = mysum[i-1] + fabs(myM[i].value)-h[i]*alpha;
      int left = 1;
      int right = len;
      int mid;

      while (right >= left)
        {
          mid = (left+right)/2;

          double tmp = ans[mid]*(i-sumlen[mid-1])-(-mysum[sumlen[mid-1]]+mysum[i]);

          if (tmp == 0 || ans[mid] == 0)
            {
              break;
            }
          else if (tmp > 0)
            {
              left = mid+1;
            }
          else
            {
              right = mid-1;
            }
        }

      if (left > right)
        {
          len = right+1;
          sumlen[len] = i;
          ans[len] = (mysum[i]-mysum[sumlen[len-1]]>0)?(mysum[i]-mysum[sumlen[len-1]]):0;
          ans[len] /= (i-sumlen[len-1]);
        }
      else
        {
          len = mid;
          ans[mid] = (mysum[i]-mysum[sumlen[mid-1]]>0)?(mysum[i]-mysum[sumlen[mid-1]]):0;
          sumlen[mid] = i;
          ans[mid] /= (i-sumlen[mid-1]);
        }
    }

  myscore[id].value = 0;
  myscore[id].index = id;
  
  for (int i=1; i<=len; ++i)
    {
      for (int j=sumlen[i-1]+1; j<=sumlen[i]; ++j)
        {
          if (myM[j].value > 0)
            {
              myans[myM[j].index] = ans[i];
            }
          else
            {
              myans[myM[j].index] = 0-ans[i];
            }

          myscore[id].value += ans[i]*old_h[j];
        }
    }

  myans[id] = M[id];

  delete []sumlen;
  delete []mysum;
  delete []myM;
  delete []ans;
}

void NON_SYMMETRIC_OPT(int n, double **myans, double **M, double *h, double *th, double *g, double rho, double alpha, double beta, struct RANK_SCORE *myscore)
{
  double **tmph;
  tmph = new double *[n+1];

  for (int i=0; i<n+1; ++i)
    {
      tmph[i] = new double [n+1];
    }

  for (int i=1; i<n+1; ++i)
    {
      for (int j=1; j<n; ++j)
        {
	  tmph[i][j] = alpha*h[j] + beta*g[i]*th[j];
        }

      ROW_SCORE(n,i,myans[i],M[i],tmph[i],rho,1,myscore,th);
    }

  for (int i=0; i<n+1; ++i)
    {
      delete [] tmph[i];
    }

  delete []tmph;
}

void DD(int n, double **myans, double **M, double *ph, double *h, double *g, double rho, double alpha, double theta, struct RANK_SCORE *myscore)
{
  srand(time(NULL));

  int **check_sign;
  double **step_size;
  double **beta;
  bool break_sign;

  check_sign = new int *[n+1];
  step_size = new double *[n+1];
  beta = new double *[n+1];
  for (int i=0; i<n+1; ++i)
    {
      beta[i] = new double [n+1];
      step_size[i] = new double [n+1];
      check_sign[i] = new int [n+1];
    }

  for (int i=0; i<n+1; ++i)
    {
      for (int j=0; j<n+1; ++j)
        {
          beta[i][j] = M[i][j];
          step_size[i][j] = DD_STEPSIZE;
          check_sign[i][j] = 0;
        }
    }

  for (int i=0; i<DD_ITER; ++i)
    {
      break_sign = true;

      NON_SYMMETRIC_OPT(n, myans, beta, ph, h, g, rho, alpha, theta, myscore);

      for (int j=1; j<n+1; ++j)
        {
          for (int k=j; k<n+1; ++k)
            {
              if (fabs(myans[j][k]-myans[k][j]) > DD_EPS*(fabs(myans[j][k])+fabs(myans[k][j])))
                {
                  break_sign = false;
                }

              if ((myans[j][k]-myans[k][j])*check_sign[j][k] > 0)
                {
                  step_size[j][k] = step_size[j][k]*(2*DD_CHANGE*(1.0+0.01*(rand()%100)));
                }
              else if ((myans[j][k]-myans[k][j])*check_sign[j][k] < 0)
                {
                  step_size[j][k] = step_size[j][k]/(2*DD_CHANGE*(1.0+0.01*(rand()%100)));
                }

              if (myans[j][k] - myans[k][j] > 0)
                {
                  check_sign[j][k] = 1;
                }
              else if (myans[j][k] - myans[k][j] < 0)
                {
                  check_sign[j][k] = -1;
                }
              else
                { 
                  check_sign[j][k] = 0;
                }

              beta[j][k] -= 2*step_size[j][k]/rho*(myans[j][k]-myans[k][j]);
              beta[k][j] += 2*step_size[j][k]/rho*(myans[j][k]-myans[k][j]);
            }
        }

      if (break_sign)
        {
          break;
        }
    }

  for (int i=1; i<n+1; ++i)
    {
      for (int j=i; j<n+1; ++j)
        {
          myans[i][j] = myans[j][i] = (myans[i][j]+myans[j][i])/2;
        }
    }

  for (int i=0; i<n+1; ++i)
    {
      delete [] check_sign[i];
      delete [] step_size[i];
      delete [] beta[i];
    }
  delete []check_sign;
  delete []step_size;
  delete []beta;
}

void ADJUST(int n, double **myans, double **M, double *h, double *th, double *g, double rho, double alpha, double theta, double ratio)
{
  srand(time(NULL));

  double asize = 0.5;
  int alimit = 200;
  struct RANK_SCORE *sort_score_1;
  struct RANK_SCORE *sort_score_2;
  double *beta;
  double *penaltyg;
  double *nodeg;
  double *mysize;
  double *mydiff;

  sort_score_1 = new struct RANK_SCORE [n+2];
  sort_score_2 = new struct RANK_SCORE [n+2];

  penaltyg = new double [n+2];
  nodeg = new double [n+2];
  beta = new double [n+2];
  mysize = new double [n+2];
  mydiff = new double [n+2];

  for (int i=1; i<n+1; ++i)
    { 
      beta[i] = 0;
      mydiff[i] = 0;
      mysize[i] = asize;
    }

  CAL_SCORE(n, M, h, sort_score_1);
  sort(sort_score_1+1, sort_score_1+n+1, cmp_score);
  for (int i=1; i<n+1; ++i)
    { 
      penaltyg[sort_score_1[i].index] = g[n-i+1];
    }

  bool pre_sign = false;
  bool curr_sign;

  for (int iter = 0; iter < alimit; ++iter)
    {
      curr_sign = true;

      DD(n, myans, M, h, th, penaltyg, rho, alpha, theta, sort_score_1);

      for (int i=1; i<n+1; ++i)
        { 
          sort_score_1[i].value += beta[i];
        }
      sort(sort_score_1+1, sort_score_1+n+1, cmp_score);
      for (int i=1; i<n+1; ++i)
        { 
          penaltyg[sort_score_1[i].index] = g[n-i+1];
        }
      CAL_SCORE(n, myans, h, sort_score_2);
      sort(sort_score_2+1, sort_score_2+n+1, cmp_score);
      for (int i=1; i<n+1; ++i)
        {
          nodeg[sort_score_2[i].index] = g[n-i+1];
        }
      for (int i=1; i<n+1; ++i)
        { 
          if ((penaltyg[i] - nodeg[i])*mydiff[i] > 0)
            { 
              mysize[i] *= (1.3*(1.0+0.005*(rand()%100)));
            }
          else if ((penaltyg[i] - nodeg[i])*mydiff[i] < 0)
            { 
              mysize[i] /= (1.0+0.005*(rand()%100));
            }

          beta[i] += mysize[i]*(penaltyg[i]-nodeg[i]);
          mydiff[i] = penaltyg[i] - nodeg[i];

          if (penaltyg[i] != nodeg[i])
            { 
              curr_sign = false;
            }
        }

      if (curr_sign && pre_sign)
        {
          break;
        }

      // Just rank top nodes

      curr_sign = true;
      for (int i=1; i<n*ratio; ++i)
	{
	  if (penaltyg[sort_score_1[i].index] != nodeg[sort_score_2[i].index])
	    {
	      curr_sign = false;
	      break;
	    }
	}

      if (curr_sign && pre_sign)
	{
	  break;
	}

      // Just rank top nodes

      pre_sign = curr_sign;
    }

  delete []sort_score_1;
  delete []sort_score_2;
  delete []beta;
  delete []penaltyg;
  delete []nodeg;
  delete []mysize;
  delete []mydiff;
}

// [[Rcpp::export]]
NumericMatrix Lovasz(NumericMatrix MM, double gamma, double rho, double alpha, double beta, int recall, double ratio, double fuzzy_level, double tune_smooth)
{
	int n = MM.nrow(); // Size of matrix	
	double *h; // Lovasz extension
	int *g; // Discrete power law
	double *tg;
	double *th;
	double **ans; // Result
	double **M; // Input M
	bool symmetric = false;
	NumericMatrix res(n, n);

	h = new double[n+1];
	g = new int[n+1];
	th = new double[n+1];
	tg = new double[n+1];
	M = new double* [n+1];
	ans = new double* [n+1];
	for (int i=0; i<n+1; ++i)
	{
		ans[i] = new double[n+1];
		M[i] = new double[n+1];
	}

	GENH(n,h);
	GENP(n,gamma,g,recall,fuzzy_level);
	TH(n,th,recall,tune_smooth);
	TG(n,g,th,tg);

	for (int i=1; i<n+1; ++i)
	  {
	    for (int j=1; j<n+1; ++j)
	      {
		M[i][j] = MM(i-1, j-1);
	      }
	  }
	
	ADJUST(n, ans, M, h, th, tg, rho, alpha, beta, ratio);
	
	for (int i=1; i<n+1; ++i)
	  {
	    for (int j=1; j<n+1; ++j)
	      {
		res(i-1,j-1) = ans[i][j];	
	      }
	  }

	for (int i=0; i<n+1; ++i)
	  {
	    delete []ans[i];
	    delete []M[i];
	  }

	delete []h;
	delete []g;
	delete []tg;
	delete []th;
	delete []ans;
	delete []M;

	return res;
}

// [[Rcpp::export]]
double ComputeObj2(NumericMatrix M, NumericMatrix X, double gamma, double rho, double alpha, double beta, int recall, double ratio, double fuzzy_level, double tune_smooth)
{
	int n = M.ncol();
	double value = 0;

	double *h;
	int *g;
	double *th;
	double *tg;
	double **s;	
	
	g = new int[n+1];
	h = new double[n+1];
	tg = new double[n+1];
	th = new double[n+1];
	s = new double *[n+1];

	for (int i=0; i<n+1; ++i)
	  {
	    s[i] = new double [n+1];
	  }

	GENH(n,h);
	GENP(n,gamma,g,recall,fuzzy_level);
	TH(n,th,recall,tune_smooth);
	TG(n,g,th,tg);
	
	for (int i=0; i<n; ++i)
	  {
	    for (int j=0; j<n; ++j)
	      {
		value += (M(i,j)-X(i,j))*(M(i,j)-X(i,j));
	      }
	  }
	value /= 2;
	value *= rho;

	for (int i=0; i<n; ++i)
	  {
	    for (int j=0; j<i; ++j)
	      {
		s[i][j] = fabs(M(i,j));
	      }

	    for (int j=i+1; j<n; ++j)
	      {
		s[i][j-1] = fabs(M(i,j));
	      }

	    sort(s[i],s[i]+n-1,mycmp_double);
	  }
  
	struct RANK_SCORE r[n+2];

	for (int i=0; i<n; ++i)
	  {
	    r[i].value = 0;
	    r[i].index = i;

	    for (int k=0; k<n-1; ++k)
	      {
		r[i].value += s[i][k]*h[k+1];
	      }
	    value += alpha*r[i].value;
	  }
  
	sort(r,r+n,cmp_score);
  
	for (int i=0; i<n; ++i)
	  {
	    int id = r[i].index;

	    for (int k=0; k<n-1; ++k)
	      {
		value += beta*tg[n-i]*s[id][k]*th[k+1];
	      }
	  }

	for (int i=0; i<n+1; ++i)
	  {
	    delete [] s[i];
	  }

	delete []h;
	delete []g;
	delete []th;
	delete []tg;
	delete []s;

	return value;
}

// [[Rcpp::export]]
NumericMatrix Predict(NumericMatrix X, int n_edge)
{
	priority_queue<node, vector<node>, compare> h;
	
	int count = 0;
	int n = X.ncol();
	for(int i=0; i<n; ++i)
	{
		for(int j=i+1; j<n; ++j)
		{
			if(h.size()< static_cast<unsigned int>(n_edge) )
			h.push( node(i+1, j+1, X(i,j) ) );
			else
			{
				if(X(i,j) > h.top().val)
				{
					h.pop();
					h.push( node(i+1, j+1, X(i,j) ) );
				}
			}
			++count;
		}
	}	
	NumericMatrix res(n_edge, 3);
	count = 0;
	while(!h.empty())
	{
		node tmp = h.top();
		res(count,0) = tmp.i;
		res(count,1) = tmp.j;
		res(count,2) = tmp.val;
		h.pop();
		++count;
	}
	return res;
}

// [[Rcpp::export]]
NumericVector ValidateRes(NumericMatrix pred, NumericMatrix X)
{
	int n = pred.nrow();
	NumericVector res(n);
	for(int i=0; i<n; ++i)
	{
		if( X( pred(i,0), pred(i,1) ) > 0)
		res(i) = 1;
		else
		res(i) = 0;
	}
	return res;
}

// [[Rcpp::export]]
void RANK(NumericMatrix Precision,int recall, double fuzzy_level)
{
  int n = Precision.nrow();
  int index = 0;
  int break_index  = n*(n-1)/2;
  double optimize_error;
  ofstream out;
  struct rr *tmp;

  tmp = new struct rr [n*n];
  out.open("RESULT");

  optimize_error = ADJUST_OPTIMIZATION + (3.0-ADJUST_OPTIMIZATION)/(1.0*recall/50);

  for (int i=0; i<n; ++i)
    {
      for (int j=i+1; j<n; ++j)
        {
          tmp[index].a = i;
          tmp[index].b = j;
          tmp[index++].c = Precision(i,j);
        }
    }
  
  sort(tmp,tmp+index,mycmp_rr);
  
  for (int i=0; i<n*(n-1)/2; ++i)
    {
      if (fabs(tmp[i].c) < 1e-5 || fabs(tmp[i].c) < 0.001*fabs(tmp[0].c))
        {
          break_index = i;
          break;
        }
    }

  for (int i=0; i<break_index/fuzzy_level/optimize_error; ++i)
    {
      out<<tmp[i].a+1<<" "<<tmp[i].b+1<<endl;
    }

  delete [] tmp;

  out.close();

  return;
}
